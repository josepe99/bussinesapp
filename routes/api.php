<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//login
Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);

//operaciones para empresa
Route::resource('empresa', EmpresaController::class);